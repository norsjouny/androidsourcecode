package com.danalock.demo.utils;

import android.app.Activity;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;


import androidx.annotation.NonNull;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;

import com.danalock.demo.MainActivity;
import com.danalock.demo.R;
import com.danalock.webservices.danaserver.ApiException;
import com.danalock.webservices.danaserver.api.OAuth2Api;
import com.danalock.webservices.danaserver.api.OAuthV2Api;
import com.danalock.webservices.danaserver.model.OAuthAccessToken;
import com.polycontrol.ekey.User;

import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;


import java.util.List;

import saschpe.android.customtabs.CustomTabsHelper;
import saschpe.android.customtabs.WebViewFallback;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;
import static saschpe.android.customtabs.CustomTabsHelper.openCustomTab;

/**
 * Any and all rights to this source code and included documentation (hereinafter collectively referred to as “the Software”),
 * including without being limited to title and copyright, belongs to Poly-Control ApS.
 * <p>
 * The Software constitutes confidential material and shall be treated as such. It may not be disclosed to third parties.
 * <p>
 * The Software may be used only in connection with authorised sale of Danalock-products. The right of use includes the right
 * to copy, change, modify, merge and develop the Software. Any publishing, distribution, sub-licensing and sale of copies of the
 * Software requires transformation of the Software to object code form, as third parties may only be granted access to the Software
 * in its object code form.
 * <p>
 * The right of use must be executed with due respect to the rights of Poly-Control ApS, and all copies of the Software shall
 * be marked as copyrighted by Poly-Control ApS.
 * <p>
 * The right of use ceases, when the authorisation to sell Danalock-products expires.
 * <p>
 * If the Parties has entered into a separate agreement concerning the right of use, such agreement must be observed in relation
 * to any use of the Software regardless of this notice.
 * <p>
 * THE SOFTWARE IS PROVIDED “AS-IS” AND ANY USE OF THE SOFTWARE IS AT OWN RISK AND EXPENSE.
 * <p>
 * Poly-Control ApS has no obligations to correct or remedy possible faults or errors in the Software.
 * <p>
 * THERE ARE NO WARRANTIES ATTACHED TO THE SOFTWARE, NEITHER EXPRESS NOR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. UNDER NO CIRCUMSTANCES SHALL
 * POLY-CONTROL APS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT OR TORT, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR ANY USE OF THE SOFTWARE IN WHATEVER FORM.
 */
public class UserUtils {

    public static void retrieveNewAccessToken(Activity activity) {
        Application app = activity.getApplication();
        getAccessFromRefreshToken(activity)
                .fail(e -> {
                    ask_user_to_sign_in(app);
                });
    }

    private static void ask_user_to_sign_in(Application context) {
        String url = String.format("https://api.danalock.com/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code", MainActivity.DANALOCK_SERVICE_CLIENT_ID, MainActivity.DANALOCK_SERVICE_REDIRECT_URI);
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(context, Uri.parse(url));
    }

    private static Promise<String, ApiException, Void> getAccessFromRefreshToken(final Activity activity) {
        final DeferredObject<String, ApiException, Void> deferred = new DeferredObject<>();
        String refreshToken = User.getAuthenticationRefreshToken(activity);
        signInWithRefreshToken(refreshToken).done(oAuthAccessToken -> {
            User.saveAccessToken(activity, oAuthAccessToken.getAccessToken());
            User.saveRefreshTokenAndExpirationTime(activity, oAuthAccessToken.getRefreshToken(), oAuthAccessToken.getExpiresIn());
            PhoneUtils.retrieveAndSetLocalDmiAddress(activity);
            deferred.resolve(oAuthAccessToken.getAccessToken());
        }).fail(deferred::reject);
        return deferred.promise();
    }

    private static Promise<OAuthAccessToken, ApiException, Void> signInWithRefreshToken(String refresh_token) {
        String grantType = "refresh_token";
        String client_id = MainActivity.DANALOCK_SERVICE_CLIENT_ID;
        String client_secret = MainActivity.DANALOCK_SERVICE_CLIENT_SECRET;
        return new OAuth2Api()
                .createAccessTokenAsync(grantType, null, client_id, client_secret, null, null, null, refresh_token, null);
    }

    static Promise<OAuthAccessToken, ApiException, Void> signInWithAuthorizationCode(Activity activity, String code) {
        String grant_type = "authorization_code";
        String client_id = MainActivity.DANALOCK_SERVICE_CLIENT_ID;
        String client_secret = MainActivity.DANALOCK_SERVICE_CLIENT_SECRET;
        String redirect_uri = MainActivity.DANALOCK_SERVICE_REDIRECT_URI;
        return new OAuth2Api()
                .createAccessTokenAsync(grant_type, code, client_id, client_secret, redirect_uri, null, null, null, null)
                .done(oAuthAccessToken -> {
                    User.saveAccessToken(activity, oAuthAccessToken.getAccessToken());
                    User.saveRefreshTokenAndExpirationTime(activity, oAuthAccessToken.getRefreshToken(), oAuthAccessToken.getExpiresIn());
                    PhoneUtils.retrieveAndSetLocalDmiAddress(activity);
                }).fail(e -> {
                    DLog.e("error httpcode: ", e.getCode(), ", errBody: ", e.getResponseBody());
                });
    }

    public static void signOut(Context context) {
        new OAuthV2Api().deleteAccessTokenByTokenAsync(User.getAccessToken(context));
        User.clearAccessToken(context);
        deleteStorage(context);
    }

    private static void deleteStorage(Context context) {
    }
}
