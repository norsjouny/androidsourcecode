package com.danalock.demo.utils;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.danalock.demo._3PartyApplication;
import com.polycontrol.BluetoothLeEkeyService;
import com.polycontrol.keys.DLKey;

import org.jdeferred.Deferred;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import static com.polycontrol.BluetoothLeEkeyService.BLE_DISCONNECTED_EXTRA_STRING_DATA_MACADDESS;

/**
 * Any and all rights to this source code and included documentation (hereinafter collectively referred to as “the Software”),
 * including without being limited to title and copyright, belongs to Poly-Control ApS.
 * <p>
 * The Software constitutes confidential material and shall be treated as such. It may not be disclosed to third parties.
 * <p>
 * The Software may be used only in connection with authorised sale of Danalock-products. The right of use includes the right
 * to copy, change, modify, merge and develop the Software. Any publishing, distribution, sub-licensing and sale of copies of the
 * Software requires transformation of the Software to object code form, as third parties may only be granted access to the Software
 * in its object code form.
 * <p>
 * The right of use must be executed with due respect to the rights of Poly-Control ApS, and all copies of the Software shall
 * be marked as copyrighted by Poly-Control ApS.
 * <p>
 * The right of use ceases, when the authorisation to sell Danalock-products expires.
 * <p>
 * If the Parties has entered into a separate agreement concerning the right of use, such agreement must be observed in relation
 * to any use of the Software regardless of this notice.
 * <p>
 * THE SOFTWARE IS PROVIDED “AS-IS” AND ANY USE OF THE SOFTWARE IS AT OWN RISK AND EXPENSE.
 * <p>
 * Poly-Control ApS has no obligations to correct or remedy possible faults or errors in the Software.
 * <p>
 * THERE ARE NO WARRANTIES ATTACHED TO THE SOFTWARE, NEITHER EXPRESS NOR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. UNDER NO CIRCUMSTANCES SHALL
 * POLY-CONTROL APS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT OR TORT, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR ANY USE OF THE SOFTWARE IN WHATEVER FORM.
 */
public class BleUtils {

    private static BroadcastReceiver disconnectReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            DLog.d("BLEUTILS", "received action: BLE_DISCONNECTED, mac: "
                    + intent.getStringExtra(BLE_DISCONNECTED_EXTRA_STRING_DATA_MACADDESS) + ", status: "
                    + intent.getIntExtra(BluetoothLeEkeyService.BLE_DISCONNECTED_EXTRA_INT_DATA_STATUS, 0));

            int state = intent.getIntExtra(BluetoothLeEkeyService.BLE_DISCONNECTED_EXTRA_INT_DATA_STATUS, 0);

            boolean error = state != 0 && state != 19;

            if (deferred != null) {
                if (error) {
                    Deferred<BluetoothLeEkeyService.BleStatus, BleException, Void> temp = deferred;
                    deferred = null;
                    temp.reject(new BleException(BluetoothLeEkeyService.BleStatus.BleStackError));
                } else {
                    Deferred<BluetoothLeEkeyService.BleStatus, BleException, Void> temp = deferred;
                    deferred = null;
                    temp.reject(new BleException(BluetoothLeEkeyService.BleStatus.Disconnected));
                }
            }
        }
    };

    private static Deferred<BluetoothLeEkeyService.BleStatus, BleException, Void> deferred = new DeferredObject<>();

    public static Promise<BluetoothLeEkeyService.BleStatus, BleException, Void> connectBleDevice(Application app, String ble_mac) {
        deferred = new DeferredObject<>();

        ((_3PartyApplication) app).getBTConnectionService().bleConnect(ble_mac, DLKey.DLKeyType.V3, new BluetoothLeEkeyService.BLEConnectContinuation() {
            @Override
            public void callback(BluetoothLeEkeyService.BleStatus bleStatus) {
                if (bleStatus == null || bleStatus == BluetoothLeEkeyService.BleStatus.Connected) {
                    Deferred<BluetoothLeEkeyService.BleStatus, BleException, Void> temp = deferred;
                    deferred = null;
                    temp.resolve(bleStatus);
                }
            }
        });

        return deferred.promise();
    }

    public static void disconnectBleDevice(Context ctx) {
        ((_3PartyApplication) ctx.getApplicationContext()).getBTConnectionService().bleDisconnect();
    }

    public static void registerDisconnectReceiver(_3PartyApplication application) {
//        application.registerReceiver();
        LocalBroadcastManager.getInstance(application).registerReceiver((disconnectReceiver), new IntentFilter(BluetoothLeEkeyService.BLE_DISCONNECTED));
    }

    public static class BleException extends RuntimeException {
        private BluetoothLeEkeyService.BleStatus status;

        BleException(BluetoothLeEkeyService.BleStatus status) {
            this.status = status;
        }

        public BluetoothLeEkeyService.BleStatus getStatus() {
            return status;
        }
    }
}
