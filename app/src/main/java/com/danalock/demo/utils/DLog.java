package com.danalock.demo.utils;

import android.util.Log;

/**
 * Any and all rights to this source code and included documentation (hereinafter collectively referred to as “the Software”),
 * including without being limited to title and copyright, belongs to Poly-Control ApS.
 * <p>
 * The Software constitutes confidential material and shall be treated as such. It may not be disclosed to third parties.
 * <p>
 * The Software may be used only in connection with authorised sale of Danalock-products. The right of use includes the right
 * to copy, change, modify, merge and develop the Software. Any publishing, distribution, sublicensing and sale of copies of the
 * Software requires transformation of the Software to object code form, as third parties may only be granted access to the Software
 * in its object code form.
 * <p>
 * The right of use must be executed with due respect to the rights of Poly-Control ApS, and all copies of the Software shall
 * be marked as copyrighted by Poly-Control ApS.
 * <p>
 * The right of use ceases, when the authorisation to sell Danalock-products expires.
 * <p>
 * If the Parties has entered into a separate agreement concerning the right of use, such agreement must be observed in relation
 * to any use of the Software regardless of this notice.
 * <p>
 * THE SOFTWARE IS PROVIDED “AS-IS” AND ANY USE OF THE SOFTWARE IS AT OWN RISK AND EXPENSE.
 * <p>
 * Poly-Control ApS has no obligations to correct or remedy possible faults or errors in the Software.
 * <p>
 * THERE ARE NO WARRANTIES ATTACHED TO THE SOFTWARE, NEITHER EXPRESS NOR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. UNDER NO CIRCUMSTANCES SHALL
 * POLY-CONTROL APS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILTY, WHETHER IN CONTRACT OR TORT, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR ANY USE OF THE SOFTWARE IN WHATEVER FORM.
 */

public class DLog {

    public static String whoCalledMe() {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[4];
        int line_number = ste.getLineNumber();
        String filename = ste.getFileName();
        return "(" + filename + ":" + line_number + ")";
    }

    public static String byteListAsString(byte[] bytes) {
        if (bytes == null)
            return "[]";
        String s = "[ ";
        for (int i = 0; i < bytes.length; i++) {
            s += Integer.toHexString(0xff & bytes[i]) + ", ";
        }
        s += " ]";
        return s;
    }

    public static void i(Object... msgs) {
        String msg = concat(msgs);

        StackTraceElement ste = Thread.currentThread().getStackTrace()[3];
        int line_number = ste.getLineNumber();
        String filename = ste.getFileName();
        String methodSubstring = ste.getMethodName().substring(0, Math.min(18, ste.getMethodName().length()));

        Log.i(methodSubstring, "(" + filename + ":" + line_number + ") " + msg);
    }

    public static void d(Object... msgs) {
        String msg = concat(msgs);

        StackTraceElement ste = Thread.currentThread().getStackTrace()[3];
        int line_number = ste.getLineNumber();
        String filename = ste.getFileName();
        String methodSubstring = ste.getMethodName().substring(0, Math.min(18, ste.getMethodName().length()));

        Log.d(methodSubstring, "(" + filename + ":" + line_number + ") " + msg);
    }

    public static void e(Object... msgs) {
        String msg = concat(msgs);

        StackTraceElement ste = Thread.currentThread().getStackTrace()[3];
        int line_number = ste.getLineNumber();
        String filename = ste.getFileName();
        String methodSubstring = ste.getMethodName().substring(0, Math.min(18, ste.getMethodName().length()));

        Log.e(methodSubstring, "(" + filename + ":" + line_number + ") " + msg);
    }

    private static String concat(Object... strings) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < strings.length; i++)
            sb.append("" + strings[i]);
        return sb.toString();
    }

    public static String byteArrayToHexString(byte[] bytes) {
        return byteListAsString(bytes);
    }
}
