package com.danalock.demo.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.danalock.demo.R;

/**
 * Created by Meraj Khan on 7/31/2019.
 */
public class ProgressDialogUtil {
    static ProgressDialog mProgressDialog;
    public static void showProgress(Context ctx, boolean cancelable) {
        try {

            mProgressDialog = new ProgressDialog(ctx);
            mProgressDialog.setMessage(ctx.getString(R.string.progress_message));
            mProgressDialog.setCanceledOnTouchOutside(cancelable);
            mProgressDialog.setCancelable(cancelable);
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog.show();
            } else if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }


        } catch (Exception e) {
            logError(e, ctx);
        }

    }

    public static void logError(Throwable e, Context ctx) {
        StringBuffer buf = new StringBuffer();
        if (e.getStackTrace() != null && e.getStackTrace().length > 0) {
            for (int iter = 0; iter < e.getStackTrace().length; iter++) {
                buf.append(e.getStackTrace()[iter].toString() + "\n");
            }
            Log.e("", buf.toString());
        } else {
            Log.e("", ctx.getString(R.string.error_no_trace));
        }
    }

    public static void cancelProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

}
