package com.danalock.demo.utils;

import android.app.Activity;
import android.content.Context;

import com.danalock.webservices.danaserver.ApiException;
import com.danalock.webservices.danaserver.DanaServerApiFactory;
import com.danalock.webservices.danaserver.api.EkeyV3Api;
import com.danalock.webservices.danaserver.model.ClientAddress;
import com.poly_control.dmi.Dmi;
import com.poly_control.dmi.Dmi_SerialNumber;
import com.polycontrol.ekey.User;

import org.jdeferred.Promise;

/**
 * Any and all rights to this source code and included documentation (hereinafter collectively referred to as “the Software”),
 * including without being limited to title and copyright, belongs to Poly-Control ApS.
 * <p>
 * The Software constitutes confidential material and shall be treated as such. It may not be disclosed to third parties.
 * <p>
 * The Software may be used only in connection with authorised sale of Danalock-products. The right of use includes the right
 * to copy, change, modify, merge and develop the Software. Any publishing, distribution, sub-licensing and sale of copies of the
 * Software requires transformation of the Software to object code form, as third parties may only be granted access to the Software
 * in its object code form.
 * <p>
 * The right of use must be executed with due respect to the rights of Poly-Control ApS, and all copies of the Software shall
 * be marked as copyrighted by Poly-Control ApS.
 * <p>
 * The right of use ceases, when the authorisation to sell Danalock-products expires.
 * <p>
 * If the Parties has entered into a separate agreement concerning the right of use, such agreement must be observed in relation
 * to any use of the Software regardless of this notice.
 * <p>
 * THE SOFTWARE IS PROVIDED “AS-IS” AND ANY USE OF THE SOFTWARE IS AT OWN RISK AND EXPENSE.
 * <p>
 * Poly-Control ApS has no obligations to correct or remedy possible faults or errors in the Software.
 * <p>
 * THERE ARE NO WARRANTIES ATTACHED TO THE SOFTWARE, NEITHER EXPRESS NOR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. UNDER NO CIRCUMSTANCES SHALL
 * POLY-CONTROL APS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT OR TORT, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR ANY USE OF THE SOFTWARE IN WHATEVER FORM.
 */
class PhoneUtils {
    private static Dmi_SerialNumber getLocalDmiSerialNumber(Context context) {
        String serial = User.getLocalSerialNumber(context);
        return serial != null ? new Dmi_SerialNumber(serial) : null;
    }

    static void retrieveAndSetLocalDmiAddress(Activity activity) {
        Dmi_SerialNumber serial = PhoneUtils.getLocalDmiSerialNumber(activity);
        if (serial != null && !serial.colonSeparatedString().equalsIgnoreCase("00:00:00:00:00:00")) {
            DLog.d("Setting local Dmi Serial number: " + serial.colonSeparatedString());
            Dmi.updateLocalDmiAddress(serial);
        } else {
            PhoneUtils.requestLocalSerialNumber(activity)
                    .done(address -> {
                        DLog.d("Setting local Dmi Serial number: " + address.getSerialNumber());
                        User.saveLocalSerialNumber(activity, address.getSerialNumber());
                        Dmi.updateLocalDmiAddress(new Dmi_SerialNumber(address.getSerialNumber()));
                    }).fail(
                    Throwable::printStackTrace
            );
        }
    }

    private static Promise<ClientAddress, ApiException, Void> requestLocalSerialNumber(final Activity context) {
        EkeyV3Api api = DanaServerApiFactory.getEkeyV3Api(context);
        return api.getNetworkAddressAsync()
                .done(result ->
                        User.saveLocalSerialNumber(context, result.getSerialNumber())
                );
    }
}
