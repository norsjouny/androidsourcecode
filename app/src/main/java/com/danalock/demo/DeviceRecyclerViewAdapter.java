package com.danalock.demo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danalock.webservices.danaserver.model.Device;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Any and all rights to this source code and included documentation (hereinafter collectively referred to as “the Software”),
 * including without being limited to title and copyright, belongs to Poly-Control ApS.
 * <p>
 * The Software constitutes confidential material and shall be treated as such. It may not be disclosed to third parties.
 * <p>
 * The Software may be used only in connection with authorised sale of Danalock-products. The right of use includes the right
 * to copy, change, modify, merge and develop the Software. Any publishing, distribution, sub-licensing and sale of copies of the
 * Software requires transformation of the Software to object code form, as third parties may only be granted access to the Software
 * in its object code form.
 * <p>
 * The right of use must be executed with due respect to the rights of Poly-Control ApS, and all copies of the Software shall
 * be marked as copyrighted by Poly-Control ApS.
 * <p>
 * The right of use ceases, when the authorisation to sell Danalock-products expires.
 * <p>
 * If the Parties has entered into a separate agreement concerning the right of use, such agreement must be observed in relation
 * to any use of the Software regardless of this notice.
 * <p>
 * THE SOFTWARE IS PROVIDED “AS-IS” AND ANY USE OF THE SOFTWARE IS AT OWN RISK AND EXPENSE.
 * <p>
 * Poly-Control ApS has no obligations to correct or remedy possible faults or errors in the Software.
 * <p>
 * THERE ARE NO WARRANTIES ATTACHED TO THE SOFTWARE, NEITHER EXPRESS NOR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. UNDER NO CIRCUMSTANCES SHALL
 * POLY-CONTROL APS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT OR TORT, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR ANY USE OF THE SOFTWARE IN WHATEVER FORM.
 */
public class DeviceRecyclerViewAdapter extends RecyclerView.Adapter<DeviceRecyclerViewAdapter.DeviceHolder> {

    public interface ListInteractionInterface {
        void listItemClicked(Device device);
    }

    private List<Device> mDevices;
    private ListInteractionInterface mInteractionListener;

    DeviceRecyclerViewAdapter(List<Device> devices, ListInteractionInterface activity) {
        this.mDevices = devices;
        this.mInteractionListener = activity;
    }

    @NonNull
    @Override
    public DeviceRecyclerViewAdapter.DeviceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_lock, parent, false);
        return new DeviceHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceHolder holder, int position) {
        Device dev = holder.device = mDevices.get(position);
        holder.tvDeviceSerialNumber.setText(dev.getSerialNumber());
        holder.tvDeviceAlias.setText(dev.getName());
        holder.tvDeviceType.setText(dev.getDeviceType());
        holder.mView.setOnClickListener(getClickListener(holder.device));
    }

    @Override
    public int getItemCount() {
        return mDevices.size();
    }

    private View.OnClickListener getClickListener(final Device device) {
        return view -> mInteractionListener.listItemClicked(device);
    }

    class DeviceHolder extends RecyclerView.ViewHolder {
        final View mView;
        Device device;
        private TextView tvDeviceSerialNumber;
        private TextView tvDeviceAlias;
        private TextView tvDeviceType;

        DeviceHolder(View view) {
            super(view);
            mView = view;
            tvDeviceSerialNumber = view.findViewById(R.id.tvDmiSerial);
            tvDeviceAlias = view.findViewById(R.id.tvAlias);
            tvDeviceType = view.findViewById(R.id.tvModelType);
        }
    }
}
