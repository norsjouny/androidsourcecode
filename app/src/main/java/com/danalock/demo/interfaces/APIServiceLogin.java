package com.danalock.demo.interfaces;

import com.danalock.demo.models.BarcodeResponseModel;
import com.danalock.demo.models.Example;
import com.danalock.demo.models.PaymentFinalResponseModel;
import com.danalock.demo.models.PaymentInitiationModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Meraj Khan on 8/19/2019.
 */
public interface APIServiceLogin {

    @POST("avanza_order_ref")
    @FormUrlEncoded
    Call<Example> bankIdLogin(@FieldMap Map<String, String> fields);
}