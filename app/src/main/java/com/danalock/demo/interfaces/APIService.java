package com.danalock.demo.interfaces;

import com.danalock.demo.models.BarcodeItem;
import com.danalock.demo.models.BarcodeResponseModel;
import com.danalock.demo.models.Example;
import com.danalock.demo.models.FinalPaymentData;
import com.danalock.demo.models.FinalPaymentDataModel;
import com.danalock.demo.models.PaymentFinalResponseModel;
import com.danalock.demo.models.PaymentInitiationModel;
 import com.danalock.demo.models.ResponseModel;


import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Meraj Khan on 7/18/2019.
 */
public interface APIService {

    @POST("productApi")
    @FormUrlEncoded
    Call<BarcodeResponseModel> saveProductBarcode(@Field("barcode") String barcode);

    @POST("paymentInit")
    @FormUrlEncoded
    Call<PaymentInitiationModel>paymentInitiate(@FieldMap Map<String,String>fields);

    @POST("paymentProceed")
    @FormUrlEncoded
    Call<FinalPaymentDataModel>paymentFinal(@Field("payment_url")String paymentUrl);
}