package com.danalock.demo.interfaces;

/**
 * Created by Meraj Khan on 7/17/2019.
 */
public interface OnItemClick {
    void onClick(String value);
}
