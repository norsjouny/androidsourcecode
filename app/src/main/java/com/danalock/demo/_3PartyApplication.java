package com.danalock.demo;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.danalock.demo.utils.BleUtils;
import com.danalock.demo.utils.DLog;
import com.polycontrol.BluetoothLeEkeyService;


/**
 * Any and all rights to this source code and included documentation (hereinafter collectively referred to as “the Software”),
 * including without being limited to title and copyright, belongs to Poly-Control ApS.
 * <p>
 * The Software constitutes confidential material and shall be treated as such. It may not be disclosed to third parties.
 * <p>
 * The Software may be used only in connection with authorised sale of Danalock-products. The right of use includes the right
 * to copy, change, modify, merge and develop the Software. Any publishing, distribution, sub-licensing and sale of copies of the
 * Software requires transformation of the Software to object code form, as third parties may only be granted access to the Software
 * in its object code form.
 * <p>
 * The right of use must be executed with due respect to the rights of Poly-Control ApS, and all copies of the Software shall
 * be marked as copyrighted by Poly-Control ApS.
 * <p>
 * The right of use ceases, when the authorisation to sell Danalock-products expires.
 * <p>
 * If the Parties has entered into a separate agreement concerning the right of use, such agreement must be observed in relation
 * to any use of the Software regardless of this notice.
 * <p>
 * THE SOFTWARE IS PROVIDED “AS-IS” AND ANY USE OF THE SOFTWARE IS AT OWN RISK AND EXPENSE.
 * <p>
 * Poly-Control ApS has no obligations to correct or remedy possible faults or errors in the Software.
 * <p>
 * THERE ARE NO WARRANTIES ATTACHED TO THE SOFTWARE, NEITHER EXPRESS NOR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. UNDER NO CIRCUMSTANCES SHALL
 * POLY-CONTROL APS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT OR TORT, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR ANY USE OF THE SOFTWARE IN WHATEVER FORM.
 */
public class _3PartyApplication extends Application {

    private BluetoothLeEkeyService mService;
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            DLog.d("onServiceConnected -- Ble Connection");
            BluetoothLeEkeyService.LocalBinder binder = ((BluetoothLeEkeyService.LocalBinder) service);
            mService = binder.getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            DLog.d("onServiceDisconnected -- Ble Connection");
            mService = null;
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        initializeBleService();
    }

    public BluetoothLeEkeyService getBTConnectionService() {
        return mService;
    }

    private void initializeBleService() {
        DLog.e("Initialize Service");
        Intent intent = new Intent(this, BluetoothLeEkeyService.class);
        getApplicationContext().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        BleUtils.registerDisconnectReceiver(this);
    }
}
