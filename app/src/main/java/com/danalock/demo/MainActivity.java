package com.danalock.demo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.danalock.demo.utils.DLog;
import com.danalock.demo.utils.NetworkUtils;
import com.danalock.demo.utils.UserUtils;
import com.danalock.webservices.danaserver.DanaServerApiFactory;
import com.danalock.webservices.danaserver.api.DevicesV1Api;
import com.danalock.webservices.danaserver.model.Device;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.danalock.demo.DeviceOperationActivity.EXTRA_ARG_DMI_SERIAL_ID;

/**
 * Any and all rights to this source code and included documentation (hereinafter collectively referred to as “the Software”),
 * including without being limited to title and copyright, belongs to Poly-Control ApS.
 * <p>
 * The Software constitutes confidential material and shall be treated as such. It may not be disclosed to third parties.
 * <p>
 * The Software may be used only in connection with authorised sale of Danalock-products. The right of use includes the right
 * to copy, change, modify, merge and develop the Software. Any publishing, distribution, sub-licensing and sale of copies of the
 * Software requires transformation of the Software to object code form, as third parties may only be granted access to the Software
 * in its object code form.
 * <p>
 * The right of use must be executed with due respect to the rights of Poly-Control ApS, and all copies of the Software shall
 * be marked as copyrighted by Poly-Control ApS.
 * <p>
 * The right of use ceases, when the authorisation to sell Danalock-products expires.
 * <p>
 * If the Parties has entered into a separate agreement concerning the right of use, such agreement must be observed in relation
 * to any use of the Software regardless of this notice.
 * <p>
 * THE SOFTWARE IS PROVIDED “AS-IS” AND ANY USE OF THE SOFTWARE IS AT OWN RISK AND EXPENSE.
 * <p>
 * Poly-Control ApS has no obligations to correct or remedy possible faults or errors in the Software.
 * <p>
 * THERE ARE NO WARRANTIES ATTACHED TO THE SOFTWARE, NEITHER EXPRESS NOR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. UNDER NO CIRCUMSTANCES SHALL
 * POLY-CONTROL APS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT OR TORT, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR ANY USE OF THE SOFTWARE IN WHATEVER FORM.
 */
public class MainActivity extends AppCompatActivity implements DeviceRecyclerViewAdapter.ListInteractionInterface {

    public final static String DANALOCK_SERVICE_CLIENT_ID = "demo"; // "danalock-android"; // "<put_your_provided_client_id_here>";
    public final static String DANALOCK_SERVICE_REDIRECT_URI = "com.danalock.demo://callback"; // "<put.your.provided.redirect.uri://callback-handle>"; // remember to change this in the manifest also <provided.redirect.uri>, the intent filter android.intent.category.BROWSABLE data scheme
    public final static String DANALOCK_SERVICE_CLIENT_SECRET = null; // "<put_your_provided_client_secret_here>";

    private List<Device> mDevices = new ArrayList<>();
    private DeviceRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.list);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new DeviceRecyclerViewAdapter(mDevices, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
         retrieveDevicesFromServerAndUpdateUI();
    }

    private void retrieveDevicesFromServerAndUpdateUI() {
        if (NetworkUtils.isNetworkAvailable(this)) {
            DevicesV1Api api = DanaServerApiFactory.getDevicesV1Api(MainActivity.this);
            api.getDevicesAsync(0)
                    .done(device_list -> {
                        DLog.d("retrieved " + device_list.size() + " Devices, they are: " + device_list);
                        updateUiList(device_list);
                    }).fail(e -> {
                DLog.e("Error retrieving Devices from server " + e.getResponseBody());
                e.printStackTrace();
                UserUtils.retrieveNewAccessToken(MainActivity.this);
            });
        } else {
            Toast.makeText(getApplicationContext(), "Turn on Network !!!", Toast.LENGTH_LONG).show();
        }
    }

    private void updateUiList(final List<Device> devices) {
        runOnUiThread(() -> {
            mDevices.clear();
            mDevices.addAll(devices);
            adapter.notifyDataSetChanged();
        });
    }

    @Override
    public void listItemClicked(Device device) {
        startActivity(new Intent(this, DeviceOperationActivity.class).putExtra(EXTRA_ARG_DMI_SERIAL_ID, device.getSerialNumber()));
    }
}
