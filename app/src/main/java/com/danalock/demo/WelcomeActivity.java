package com.danalock.demo;

 import android.content.Intent;
 import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        TextView tvShopping =  findViewById(R.id.txt_shopping);

        tvShopping.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

              startActivity(new Intent(WelcomeActivity.this, ScanActivity.class));

            }
        });
    }

}
