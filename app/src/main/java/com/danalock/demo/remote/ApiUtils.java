package com.danalock.demo.remote;

import com.danalock.demo.interfaces.APIService;
import com.danalock.demo.interfaces.APIconstants;

/**
 * Created by Meraj Khan on 7/18/2019.
 */
public class ApiUtils {
    private ApiUtils() {}

  //  public static final String Hello_URL = "http://norsjo.stagingshop.com/api/";
   // http://norsjo.stagingshop.com/api/paymentInit
    public static APIService getAPIService2() {

        return RetrofitClient.getClient(APIconstants.PRODUCT_URL).create(APIService.class);
    }
}