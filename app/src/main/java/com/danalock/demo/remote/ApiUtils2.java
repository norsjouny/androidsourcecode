package com.danalock.demo.remote;


import com.danalock.demo.interfaces.APIServiceLogin;
import com.danalock.demo.interfaces.APIconstants;

/**
 * Created by Meraj Khan on 7/19/2019.
 */
public class ApiUtils2 {
    private ApiUtils2() {}

  /* *//*=============Live BankId===================*//*
  //  public static final String BASE_URL = "http://52.66.58.148:9099/1.0/api/";


     *//* =============Stagging BankID===============*//*

     public static final String BASE_URL = "http://52.66.58.148:9095/1.0/api/";*/

    public static APIServiceLogin getAPIService() {

        return RetrofitClient.getClient(APIconstants.BASE_URL).create(APIServiceLogin.class);
    }
}