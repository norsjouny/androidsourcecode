package com.danalock.demo.remote;



import java.util.concurrent.TimeUnit;

import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Meraj Khan on 7/18/2019.
 */

public class RetrofitClient {

    private static Retrofit retrofit = null;
    private static Dispatcher dispatcher = null;


    public static Retrofit getClient(String baseUrl) {
      //  if (retrofit == null) {

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

            httpClient.readTimeout(5, TimeUnit.MINUTES);
            httpClient.connectTimeout(5, TimeUnit.MINUTES);
            httpClient.writeTimeout(5, TimeUnit.MINUTES);
            OkHttpClient client = httpClient.addInterceptor(interceptor).build();

            dispatcher = new Dispatcher();
            dispatcher.setMaxRequests(150);
            httpClient.dispatcher(dispatcher);


            Retrofit.Builder builder =
                    new Retrofit.Builder()
                            .baseUrl(baseUrl)
                            .client(client)
                            .addConverterFactory(GsonConverterFactory.create());
            retrofit = builder.build();
// Service servicee = retrofit.create(serviceClass);

        //}
        return retrofit;
    }

    public static Dispatcher getDispatcher() {
        return dispatcher;
    }

    public static void cancelAllTask() {
        if (dispatcher != null) {
            dispatcher.cancelAll();
        }
    }



}



