package com.danalock.demo;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.webkit.WebView;

public class WebViewActivity extends AppCompatActivity {
    private WebView webViews;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        webViews =   findViewById(R.id.webView1);
        webViews.getSettings().setJavaScriptEnabled(true);
        String url = String.format("https://api.danalock.com/oauth2/authorize?client_id=%s&redirect_uri=%s&response_type=code", MainActivity.DANALOCK_SERVICE_CLIENT_ID, MainActivity.DANALOCK_SERVICE_REDIRECT_URI);

        webViews.loadUrl(url);
    }
}
