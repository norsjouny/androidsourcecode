package com.danalock.demo;

 import android.content.Intent;
 import android.os.Bundle;
 import android.view.View;
 import android.widget.Button;
 import android.widget.TextView;

 import androidx.appcompat.app.AppCompatActivity;

public class ThanksActivity extends AppCompatActivity implements View.OnClickListener {
   private  TextView textView;
   private  Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanks);
         textView =findViewById(R.id.order_id);
         button     =findViewById(R.id.btn_go_shopping);
        String orderId = getIntent().getStringExtra("ORDER_ID");
        if(orderId!=null) {
            textView.setText(orderId);
        }else {
            textView.setText(" ");
        }
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

         startActivity(new Intent(ThanksActivity.this,ScanActivity.class));
         finish();
    }
}
