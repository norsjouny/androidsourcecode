package com.danalock.demo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.danalock.demo.adapters.CartAdapter;
import com.danalock.demo.adapters.RecyclerViewAdapter;
import com.danalock.demo.interfaces.APIService;
import com.danalock.demo.models.BarcodeResponseModel;
import com.danalock.demo.models.Data;
import com.danalock.demo.models.FinalPaymentData;
import com.danalock.demo.models.FinalPaymentDataModel;
import com.danalock.demo.models.PaymentFinalResponseModel;
import com.danalock.demo.models.PaymentInitiationModel;
import com.danalock.demo.remote.ApiUtils;
import com.danalock.demo.utils.NetworkUtils;
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScanner;
import com.edwardvanraak.materialbarcodescanner.MaterialBarcodeScannerBuilder;
import com.google.android.gms.vision.barcode.Barcode;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScanActivity extends AppCompatActivity implements View.OnClickListener {
    //View Objects
    private Button buttonScan, buttonCheckout;
    private TextView textViewName, textViewAddress, grandTotal;
    private Barcode barcodeResult;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter mAdapter;
    private  CartAdapter adapter;
    private ProgressDialog progressDoalog;
    private String token;
    private String paymentRequestToken;
    private String paymentUrl;
    ListView productOrderList;
   // public final static String NORSJO_SERVICE_REDIRECT_URI = "com.danalock.norsjoab://callback";
    public final static String NORSJO_SERVICE_REDIRECT_URI = "com.danalock.demo://callback";
    private List<Data> productList = new ArrayList<>();
    DataSetObserver observer = new DataSetObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            setMealTotal();
        }
    };
    private ProgressDialog mProgressDialog;
    private APIService mAPIService;

    //qr code scanner object
// productList==orders
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_scan);
        productOrderList = findViewById(R.id.products_recyclerview);
        grandTotal = findViewById(R.id.tv_total);
        // prepareProductdata();
        mAPIService = ApiUtils.getAPIService2();

        buttonScan = findViewById(R.id.buttonScan);
        buttonCheckout = findViewById(R.id.buttoncheckout);
        // textViewName = (TextView) findViewById(R.id.textViewName);
        // textViewAddress = (TextView) findViewById(R.id.textViewAddress);
        //intializing scan object
        //attaching onclick listener
        buttonScan.setOnClickListener(this);
        buttonCheckout.setOnClickListener(this);
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo(" com.danalock.demo", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
    }
    public int calculateGrandTotal() {
        int mealTotal = 0;
        for (Data order : productList) {
            mealTotal += Integer.parseInt(order.getPrice()) * order.getUnits();
        }
        return mealTotal;
    }
    public void setMealTotal() {
        grandTotal.setText(calculateGrandTotal() + " " + "Kr");
    }

    private void showDialog(final String scanContent) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ScanActivity.this);
        builder.setMessage(scanContent)
                .setTitle(R.string.dialog_title);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(ScanActivity.this, "Saved", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(ScanActivity.this, "Not Saved", Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonScan:
                //initiating the qr code scan
               //qrScan.initiateScan();
                final MaterialBarcodeScanner materialBarcodeScanner = new MaterialBarcodeScannerBuilder()
                        .withActivity(ScanActivity.this)
                        .withEnableAutoFocus(true)
                        .withBleepEnabled(true)
                        .withBackfacingCamera()
                        .withCenterTracker()
                        .withText("Place the barcode inside focus area")
                        .withResultListener(new MaterialBarcodeScanner.OnResultListener() {
                            @Override
                            public void onResult(Barcode barcode) {
                                barcodeResult = barcode;
                                String BarcodeData = barcode.rawValue;
                                String tempBarcode= BarcodeData;
                                ArrayList<String> uniqueList=new ArrayList<>();
                                uniqueList.add(BarcodeData);
                                progressDoalog = new ProgressDialog(ScanActivity.this);
                                //progressDoalog.setMax(100);
                                progressDoalog.setMessage("Loading....");
                                progressDoalog.setTitle("Loading Product ");
                                progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                                // show it
                                progressDoalog.show();
                                if (NetworkUtils.isNetworkAvailable(ScanActivity.this)) {
                                    sendPostBarcode(BarcodeData);
                                    Toast.makeText(getApplicationContext(),"Scanned Value is: "+BarcodeData,Toast.LENGTH_LONG).show();
                                }else{

                                    Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_LONG).show();
                                }

                            }
                        })
                        .build();
                materialBarcodeScanner.startScan();
                    break;
            case R.id.buttoncheckout:

                if (NetworkUtils.isNetworkAvailable(ScanActivity.this)) {
                    if(calculateGrandTotal()>0){
                        Map<String, String> map = new HashMap<>();
                        map.put("payeePaymentReference", "123456");
                        map.put("payeeAlias", "1236569453");
                        // map.put("payeeAlias", "1231141189");
                        map.put("amount", ""+calculateGrandTotal());
                        map.put("message", "Payment is requesting");
                        progressDoalog = new ProgressDialog(ScanActivity.this);
                        progressDoalog.setMessage("Payment Initiation Started....");
                        progressDoalog.setTitle("Initiating Payment");
                        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        // show it
                        progressDoalog.show();
                        paymentInitiation(map);
                        break;
                    }else{
                        Toast.makeText(getApplicationContext(),"Please add quantity to buy",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_LONG).show();
                }
        }
    }
    public void paymentInitiation (Map map){
        mAPIService.paymentInitiate(map).enqueue(new Callback<PaymentInitiationModel>(){
            @Override
            public void onResponse(Call<PaymentInitiationModel> call, Response<PaymentInitiationModel> response) {
                if(response.body().status.equals("200")){
                   // Toast.makeText(getApplicationContext(),"Payment initiation started ",Toast.LENGTH_LONG).show();
                     progressDoalog.dismiss();
                    //Map<String ,String> map = new HashMap<>();
                    //map.put("payment_url ", response.body().data);
                     paymentUrl=response.body().data;
                    paymentRequestToken=response.body().PaymentRequestToken;
                    int index = paymentUrl.lastIndexOf('/');
                    token = paymentUrl.substring(index +1);
                   // NORSJO_SERVICE_REDIRECT_URI
                 //   finalPayment(paymentUrl.trim());
                    if(isSwishInstalled(getApplicationContext())){
                        progressDoalog = new ProgressDialog(ScanActivity.this);
                        progressDoalog.setMessage("Payment Finalization Started....");
                        progressDoalog.setTitle("Finalizing  Payment");
                        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        // show it
                        progressDoalog.show();
                        openSwishWithToken(getApplicationContext(),paymentRequestToken,NORSJO_SERVICE_REDIRECT_URI);
                        progressDoalog.dismiss();
                    }else{
                        Toast.makeText(getApplicationContext(),"Please install Swish app ",Toast.LENGTH_LONG).show();
                        progressDoalog.dismiss();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<PaymentInitiationModel> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(getApplicationContext(),"Payment initiation Failed ",Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(progressDoalog!=null)
        progressDoalog.dismiss();
        if(paymentUrl!=null)
        finalPayment(paymentUrl);
    }

    public void finalPayment(String url){


        mAPIService.paymentFinal(url).enqueue(new Callback<FinalPaymentDataModel>(){

            @Override
            public void onResponse(Call<FinalPaymentDataModel> call, Response<FinalPaymentDataModel> response) {

                if(response.body().data.status.equalsIgnoreCase("PAID")){
                    String orderIdstr= response.body().data.id;
                    Toast.makeText(getApplicationContext(),response.body().data.message,Toast.LENGTH_LONG).show();
                    progressDoalog.dismiss();
                    Intent intent = new Intent(getBaseContext(), ThanksActivity.class);
                    intent.putExtra("ORDER_ID", orderIdstr);
                    startActivity(intent);
                    finish();
                   //startActivity(new Intent(ScanActivity.this, ThanksActivity.class));
                   //finish();

                }else if(response.body().data.status.equalsIgnoreCase("CREATED")){
                    progressDoalog.dismiss();
                    Toast.makeText(getApplicationContext(),response.body().data.message,Toast.LENGTH_LONG).show();
                }else{
                    progressDoalog.dismiss();
                    Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void onFailure(Call<FinalPaymentDataModel> call, Throwable t) {
                progressDoalog.dismiss();
                Toast.makeText(getApplicationContext(),"Payment Failed-->"+t,Toast.LENGTH_LONG).show();
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });

    }
            //==============check for swish app=================
    public static boolean isSwishInstalled(Context context) {
        try {
            context.getPackageManager()
                    .getPackageInfo("se.bankgirot.swish", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            // Swish app is not installed
            return false;
        }
    }

    public static boolean openSwishWithToken(Context context, String token, String callBackUrl) {
        if ( token == null || token.length() == 0 || callBackUrl == null || callBackUrl.length() == 0 || context == null) {
            return false;
        }
        // Construct the uri
        // Note that appendQueryParameter takes care of uri encoding
        // the parameters

        Uri url = new Uri.Builder()
                .scheme("swish")
                .authority("paymentrequest")
                .appendQueryParameter("token", token)
                .appendQueryParameter("callbackurl", callBackUrl)
                .build();
        Intent intent = new Intent(Intent.ACTION_VIEW, url);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setPackage("se.bankgirot.swish");
        try {
            context.startActivity(intent);
        } catch (Exception e){
            // Unable to start Swish
            Toast.makeText(context,"Unable to start Swish",Toast.LENGTH_LONG).show();

            return false;
        }
        return true;
    }

    public void sendPostBarcode(String barcode) {
        mAPIService.saveProductBarcode(barcode).enqueue(new Callback<BarcodeResponseModel>() {
            @Override
            public void onResponse(Call<BarcodeResponseModel> call, Response<BarcodeResponseModel> response) {
                if (response.isSuccessful()) {

                    Data data = response.body().data;

                    progressDoalog.dismiss();
                    if (response.body().status.equals("200")) {
                        productList.add(data);
                        adapter  = new CartAdapter(getApplicationContext(), productList);
                        productOrderList.setAdapter(adapter);
                        adapter.registerDataSetObserver(observer);
                       // Intent intent = getIntent();
                       // startActivity(intent);
                        Log.d("success is response", "sssss");
                    } else if (response.body().status.equals("201")) {
                        Log.d("failed is response", "no data will be there !!!!!!!");
                        Toast.makeText(getApplicationContext(),"Unable to get Details",Toast.LENGTH_LONG).show();

                    }

                   /* if (mProgressDialog.isShowing()) {
                        mProgressDialog.dismiss();
                        ((Activity) getApplicationContext()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }*/
                    // showResponse(response.body().toString());
                    Log.d("post submitted to API ", response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<BarcodeResponseModel> call, Throwable t) {
                Log.e("Unable to submit...", " dddd");
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    ((Activity) getApplicationContext()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            }
        });
    }

//    public void showResponse(String response) {
//        if (mResponseTv.getVisibility() == View.GONE) {
//            mResponseTv.setVisibility(View.VISIBLE);
//        }
//        mResponseTv.setText(response);
//    }


    public class sendBarcodeToserver extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            return null;
        }

        @Override
        protected void onPostExecute(String temp) {

        }
    }
}