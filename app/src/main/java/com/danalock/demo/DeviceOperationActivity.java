package com.danalock.demo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.danalock.demo.utils.BleUtils;
import com.danalock.demo.utils.DLog;
import com.danalock.demo.utils.UserUtils;
import com.danalock.webservices.danaserver.ApiException;
import com.danalock.webservices.danaserver.DanaServerApiFactory;
import com.danalock.webservices.danaserver.model.LoginToken;
import com.polycontrol.BluetoothLeEkeyService;
import com.polycontrol.blescans.DLBleScanData;
import com.polycontrol.devices.models.DLDevice;
import com.polycontrol.devices.models.DMILock;
import com.polycontrol.keys.DLKey;
import com.polycontrol.keys.DLV3Key;
import com.polycontrol.keys.DLV3LoginToken;

import org.jdeferred.Promise;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Any and all rights to this source code and included documentation (hereinafter collectively referred to as “the Software”),
 * including without being limited to title and copyright, belongs to Poly-Control ApS.
 * <p>
 * The Software constitutes confidential material and shall be treated as such. It may not be disclosed to third parties.
 * <p>
 * The Software may be used only in connection with authorised sale of Danalock-products. The right of use includes the right
 * to copy, change, modify, merge and develop the Software. Any publishing, distribution, sub-licensing and sale of copies of the
 * Software requires transformation of the Software to object code form, as third parties may only be granted access to the Software
 * in its object code form.
 * <p>
 * The right of use must be executed with due respect to the rights of Poly-Control ApS, and all copies of the Software shall
 * be marked as copyrighted by Poly-Control ApS.
 * <p>
 * The right of use ceases, when the authorisation to sell Danalock-products expires.
 * <p>
 * If the Parties has entered into a separate agreement concerning the right of use, such agreement must be observed in relation
 * to any use of the Software regardless of this notice.
 * <p>
 * THE SOFTWARE IS PROVIDED “AS-IS” AND ANY USE OF THE SOFTWARE IS AT OWN RISK AND EXPENSE.
 * <p>
 * Poly-Control ApS has no obligations to correct or remedy possible faults or errors in the Software.
 * <p>
 * THERE ARE NO WARRANTIES ATTACHED TO THE SOFTWARE, NEITHER EXPRESS NOR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. UNDER NO CIRCUMSTANCES SHALL
 * POLY-CONTROL APS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT OR TORT, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR ANY USE OF THE SOFTWARE IN WHATEVER FORM.
 */
public class DeviceOperationActivity extends AppCompatActivity {

    public final static String EXTRA_ARG_DMI_SERIAL_ID = "extra_arg_device_id";

    private DLDevice dlDevice;
    private LoginToken token;
    private String ble_mac_address;
    private String deviceId;
    private ProgressDialog progressDoalog;
    private Button btnUnlock;
    private Button btnLock;
    //The "x" and "y" position of the "Show Button" on screen.
    Point p;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_operation);
        BleScanner.requestLocationPermissionsIfNeeded(DeviceOperationActivity.this);
        btnUnlock= findViewById(R.id.btnLock_UnlockOperation);
        btnLock= findViewById(R.id.btnLock_LockOperation);
        deviceId = getIntent().getStringExtra(EXTRA_ARG_DMI_SERIAL_ID);
        setupUiForDevice(deviceId);
    }
    private void setupUiForDevice(String deviceId) {
        getLoginTokenForDeviceFromServer(deviceId)
                .done(result -> {
                    token = result;
                    DLV3LoginToken dlkey = (DLV3LoginToken) DLV3Key.getInstance(token);
                    dlDevice = DLDevice.getDevice(dlkey);
                    if (dlDevice instanceof DMILock) { // either a Universal V3 module or a DanalockV3
                        updateLabels(dlkey);
                        enableLockClickListeners();
                    } else {
                        DLog.d("DeviceOp", "this is not a DMILock, it's a " + token.getDevice().getDeviceType());
//                            if (dlDevice instanceof DanaHWBridge) {
//                                enableBridgeClickListeners();
//                            } else if (dlDevice instanceof DanaPadV3) {
//                                enableNumPadClickListeners();
//                            }
                    }
                });
    }
    private void updateLabels(DLV3LoginToken dlkey) {
        // enable UI device buttons after a bluetooth scan to see state of device...
        String alias = dlkey.getAlias();
        Boolean  ss= dlkey.canAutoCalibrate();
        DLog.e("LOOOOOOOOOOOOOK",alias);
        runOnUiThread(() -> {
             findViewById(R.id.btnLock_UnlockOperation);
             findViewById(R.id.btnLock_UnlockOperation);
        });
    }
    private void enableLockClickListeners() {
        findViewById(R.id.btnLock_UnlockOperation).setOnClickListener(view -> {
            DLog.e("unlock button clicked");
            btnUnlock.setEnabled(false);
            btnLock.setEnabled(true);
            progressDoalog = new ProgressDialog(DeviceOperationActivity.this);
            //progressDoalog.setMax(100);
            progressDoalog.setMessage("Unlocking....");
            progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDoalog.setCancelable(false);
            // show it
            progressDoalog.show();
            // First
            // + find the Bluetooth mac address, then
            // + establish a bluetooth connection to the device, then
            // + do a dmi operation eg. unlock() etc. and remember to
            // + disconnect (also on fails)
            if (ble_mac_address != null) {  // Refactor and choose own way of keeping track of bluetooth mac addresses
                DLog.d("DeviceOp", "mac address is not null");
                connectAndDo_Unlock_Operation();
            } else {
              //  BleScanner.requestLocationPermissionsIfNeeded(DeviceOperationActivity.this);
                BleScanner.startBleScan(DeviceOperationActivity.this);
                DLog.d("Scanning for device");
                BleScanner.listenForDevice(
                        dlDevice.getDeviceId(),
                        new BleScanner.GetDeviceContinuation() {  // Refactor and choose own way of keeping track of bluetooth mac addresses
                            @Override
                            public void found(String macAddress, DLBleScanData scanData, int rssi) {
                                BleScanner.stopBleScan(); // on some Android phones it has been observed the Ble connection success ratio increases when ble scanning is stopped before a connection
                                ble_mac_address = macAddress;
                                dlDevice.touch(scanData, rssi, macAddress);
                                connectAndDo_Unlock_Operation();
                            }
                            @Override
                            public void timedOut() {
                                Toast.makeText(getApplicationContext(), "Timed out looking for ble device " + dlDevice.getDeviceId(), Toast.LENGTH_LONG).show();
                                startActivity(new Intent(DeviceOperationActivity.this,MainActivity.class));
                                finish();
                                DLog.e("Timed out looking for ble device " + dlDevice.getDeviceId());
                            }
                        },
                       30000L); // Depending on phone manufacture have observed up to 25 seconds before Ble has been enabled and a first Ble beacon is received by callback
            }
        });

//         private void setupEventCallbacks() {
//            final SlideToActView slide=findViewById(R.id.btnLock_UnlockOperation);
//
//            slide.setOnSlideCompleteListener(new SlideToActView.OnSlideCompleteListener() {
//                @Override
//                public void onSlideComplete(@NotNull SlideToActView slideToActView) {
//                    if (ble_mac_address != null) {  // Refactor and choose own way of keeping track of bluetooth mac addresses
//                        DLog.d("DeviceOp", "mac address is not null");
//                        connectAndDo_Lock_Operation();
//                    } else {
//                        BleScanner.requestLocationPermissionsIfNeeded(DeviceOperationActivity.this);
//                        BleScanner.startBleScan(DeviceOperationActivity.this);
//                        DLog.d("Scanning for device");
//                        BleScanner.listenForDevice(
//                                dlDevice.getDeviceId(),
//                                new BleScanner.GetDeviceContinuation() {  // Refactor and choose own way of keeping track of bluetooth mac addresses
//                                    @Override
//                                    public void found(String macAddress, DLBleScanData scanData, int rssi) {
//                                        BleScanner.stopBleScan(); // on some Android phones it has been observed the Ble connection success ratio increases when ble scanning is stopped before a connection
//                                        ble_mac_address = macAddress;
//                                        dlDevice.touch(scanData, rssi, macAddress);
//                                        connectAndDo_Lock_Operation();
//                                    }
//                                    @Override
//                                    public void timedOut() {
//                                        Toast.makeText(getApplicationContext(), "Timed out looking for ble device " + dlDevice.getDeviceId(), Toast.LENGTH_LONG).show();
//                                        DLog.e("Timed out looking for ble device " + dlDevice.getDeviceId());
//                                    }
//                                },
//                                30000L); // Depending on phone manufacture have observed up to 25 seconds before Ble has been enabled and a first Ble beacon is received by callback
//                    }
//                }
//            });
//
//        }

        findViewById(R.id.btnLock_LockOperation).setOnClickListener(view -> {
            DLog.e("lock button clicked");
            btnUnlock.setEnabled(true);
            btnLock.setEnabled(false);
            progressDoalog = new ProgressDialog(DeviceOperationActivity.this);
            //progressDoalog.setMax(100);
            progressDoalog.setMessage("Unlocking....");
            progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDoalog.setCancelable(false);
            // show it
            progressDoalog.show();

            if (ble_mac_address != null) {  // Refactor and choose own way of keeping track of bluetooth mac addresses
                DLog.d("DeviceOp", "mac address is not null");
                connectAndDo_Lock_Operation();
            } else {

                BleScanner.startBleScan(DeviceOperationActivity.this);
                DLog.d("Scanning for device");

                BleScanner.listenForDevice(
                        dlDevice.getDeviceId(),
                        new BleScanner.GetDeviceContinuation() {  // Refactor and choose own way of keeping track of bluetooth mac addresses
                            @Override
                            public void found(String macAddress, DLBleScanData scanData, int rssi) {
                                BleScanner.stopBleScan(); // on some Android phones it has been observed the Ble connection success ratio increases when ble scanning is stopped before a connection
                                ble_mac_address = macAddress;
                                dlDevice.touch(scanData, rssi, macAddress);

                                //callPopUpWindow(scanData.toString());
                                connectAndDo_Lock_Operation();
                            }
                            @Override
                            public void timedOut() {
                                Toast.makeText(getApplicationContext(), "Timed out looking for ble device " + dlDevice.getDeviceId(), Toast.LENGTH_LONG).show();
                                DLog.e("Timed out looking for ble device " + dlDevice.getDeviceId());
                                startActivity(new Intent(DeviceOperationActivity.this,MainActivity.class));
                                finish();
                            }
                        },
                        60000L); // Depending on phone manufacture have observed up to 25 seconds before Ble has been enabled and a first Ble beacon is received by callback
            }
        });
    }

/*    private void  callPopUpWindow (String lockData) {
        // inflate the layout of the popup window
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window, null);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        ((TextView)popupWindow.getContentView().findViewById(R.id.tv_popup)).setText(lockData);
        // show the popup window
        // which view you pass in doesn't matter, it is only used for the window tolken
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

        // dismiss the popup window when touched
        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popupWindow.dismiss();
                return true;
            }
        });
    }*/

    // Get the x and y position after the button is draw on screen
// (It's important to note that we can't get the position in the onCreate(),
// because at that stage most probably the view isn't drawn yet, so it will return (0, 0))

    // The method that displays the popup.
    private void showPopup(final Activity context, Point p) {
        int popupWidth = 200;
        int popupHeight = 150;
        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup_window, viewGroup);
        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);
        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
        int OFFSET_X = 30;
        int OFFSET_Y = 30;
        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());
        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, p.x + OFFSET_X, p.y + OFFSET_Y);
        // Getting a reference to Close button, and close the popup when clicked.
        Button close = (Button) layout.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });
    }


    private void connectAndDo_Unlock_Operation() {


        ((_3PartyApplication) getApplication()).getBTConnectionService()
                .bleConnect(ble_mac_address, DLKey.DLKeyType.V3, bleStatus -> {
                    DLog.d("Connected? ", ble_mac_address, ", status: ", bleStatus);

                    if (bleStatus == null || bleStatus == BluetoothLeEkeyService.BleStatus.Connected) {
                        ((DMILock) dlDevice)
                                .unlock(status -> {
                                   progressDoalog.dismiss();
                                     DLog.d("unLocked? status: ", status);
                                    BleUtils.disconnectBleDevice(getApplication());
                                    startActivity(new Intent(DeviceOperationActivity.this, WelcomeActivity.class));
                                });
                    }
                });
    }

    private void connectAndDo_Lock_Operation() {


        ((_3PartyApplication) getApplication()).getBTConnectionService()
                .bleConnect(ble_mac_address, DLKey.DLKeyType.V3, bleStatus -> {

                    DLog.d("Connected? ", ble_mac_address, ", status: ", bleStatus);
                    if (bleStatus == null || bleStatus == BluetoothLeEkeyService.BleStatus.Connected) {
                        ((DMILock) dlDevice)
                                .lock(status -> {
                                progressDoalog.dismiss();
                                     DLog.d("unLocked? status: ", status);
                                    BleUtils.disconnectBleDevice(getApplication());
                                });
                    }
                });
    }

    private Promise<LoginToken, ApiException, Void> getLoginTokenForDeviceFromServer(final String serial_number) {
        return DanaServerApiFactory.getDevicesV1Api(this)
                .getLoginTokenBySerialNumberAsync(serial_number)
                .fail(e -> {
                    Toast.makeText(getApplicationContext(), "Error retrieving key", Toast.LENGTH_LONG).show();
                    DLog.e("Error retrieving key from server, code: ", e.getCode(), ", errBody: " + e.getResponseBody());
                    e.printStackTrace();
                    UserUtils.retrieveNewAccessToken(DeviceOperationActivity.this);
                    finish();
                });
    }
}
