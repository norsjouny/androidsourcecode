package com.danalock.demo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.danalock.demo.interfaces.APIService;
import com.danalock.demo.interfaces.APIServiceLogin;
import com.danalock.demo.models.Example;
import com.danalock.demo.remote.ApiUtils2;
import com.danalock.demo.utils.NetworkUtils;
import com.danalock.demo.utils.ProgressDialogUtil;
import com.danalock.demo.utils.SharedPrefUtil;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private final String RESPONSE_KEY_SHARED_PREF = "json_response";
    private APIServiceLogin mAPIService2;
    EditText editTextPersonalNumber;
    // private String personalNumber;
    private ProgressDialog mProgressDialog;
    private String android_id;
    private String personalNumber;
    protected Context mThisActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        //getSupportActionBar().hide(); // hide the title bar
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        //WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen
        setContentView(R.layout.activity_login);
        mAPIService2 = ApiUtils2.getAPIService();
        Button btn = findViewById(R.id.btn_submit);
        editTextPersonalNumber = (EditText) findViewById(R.id.txt_bank_id);

        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        editTextPersonalNumber.setText("194210142222");
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        String ipAddress = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());

        btn.setOnClickListener(v -> {

            if (NetworkUtils.isNetworkAvailable(this)) {
                personalNumber = editTextPersonalNumber.getText().toString();
                Map<String, String> map = new HashMap<>();
                map.put("bank_id", personalNumber);
                map.put("device_id", android_id);
                map.put("device_type", "android");
                map.put("req_ip", ipAddress);

                if (personalNumber.matches("")) {
                    Toast.makeText(getApplicationContext(), "Please Enter Personal Number", Toast.LENGTH_LONG).show();
                } else {

                    Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.bankid.bus");
                    if (launchIntent != null) {
                        sendPostPersonalNumber(map);
                        ProgressDialogUtil.showProgress(getBaseContext(),false);
                        launchIntent.setAction(Intent.ACTION_VIEW);
                        launchIntent.setData(Uri.parse("bankid:///?autostarttoken=null&redirect=null "));
                        startActivity(launchIntent);//null pointer check in case package name was not found
                    } else {
                        Toast.makeText(getApplicationContext(), "Please Install BankId Application", Toast.LENGTH_LONG).show();
                    }

                }
            } else {
                Toast.makeText(getApplicationContext(), "No internet connection ", Toast.LENGTH_LONG).show();
            }

        });

    }

    public void sendPostPersonalNumber(Map map) {
        mAPIService2.bankIdLogin(map).enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                if (response.isSuccessful()) {
                    if (response.body().response != null) {

                        if (response.body().response.status.equalsIgnoreCase("2000")) {
                           // SharedPrefUtil.setUserObject(getApplicationContext(), response.toString(), RESPONSE_KEY_SHARED_PREF);



                           // SharedPrefUtil.getUserObject(getApplicationContext(), RESPONSE_KEY_SHARED_PREF);

                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                            Toast.makeText(getApplicationContext(), "Authentication Success", Toast.LENGTH_LONG).show();
                                        /* if (mProgressDialog.isShowing()) {
                                             mProgressDialog.dismiss();
                                             ((Activity) getApplicationContext()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                         }*/
                            // showResponse(response.body().toString());
                            Log.d("API response is 2000 ", response.body().toString());
                        } else if (response.body().response.status.equalsIgnoreCase("201")) {
                            Toast.makeText(getApplicationContext(), "Invalid request", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Something Went Wrong!!", Toast.LENGTH_LONG).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "No response from server ", Toast.LENGTH_LONG).show();
                //  Log.e("Unable to submit...", " dddd");
              /*  if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    ((Activity) getApplicationContext()).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }*/
            }
        });
    }
}
