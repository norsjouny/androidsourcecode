package com.danalock.demo.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Meraj Khan on 7/18/2019.
 */
public class BarcodeItem {

    @SerializedName("barcode")
     private String barcode;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
