
package com.danalock.demo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FinalPaymentDataModel {

    @SerializedName("status")

    public String status;
    @SerializedName("message")

    public String message;
    @SerializedName("data")

    public FinalPaymentData data;

}
