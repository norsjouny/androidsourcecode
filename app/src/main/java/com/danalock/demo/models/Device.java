
package com.danalock.demo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Device {

    @SerializedName("ipAddress")
    @Expose
    public String ipAddress;

}
