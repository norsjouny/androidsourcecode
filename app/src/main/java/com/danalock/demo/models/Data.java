
package com.danalock.demo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("barcode")
    @Expose
    public String barcode;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("product_desc")
    @Expose
    public String productDesc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("units")
    @Expose
    public int units;
    @SerializedName("image")
    @Expose
    public Object image;
    @SerializedName("updated_date")
    @Expose
    public String updatedDate;
    public void addToQuantity(){
        this.units += 1;
    }

    public void removeFromQuantity(){
        if( this.units  > 0){
            this.units  -= 1;
        }
    }
}
