package com.danalock.demo.models;

/**
 * Created by Meraj Khan on 7/17/2019.
 */
public class ProductModel {
        private String productName;
        private int productPrice;
        private int quantity;

    public ProductModel(String productName, int productPrice) {
        this.productName = productName;
        this.productPrice = productPrice;
        this.quantity = 0;
    }



    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public void addToQuantity(){
        this.quantity += 1;
    }

    public void removeFromQuantity(){
        if(this.quantity > 1){
            this.quantity -= 1;
        }
    }
}
