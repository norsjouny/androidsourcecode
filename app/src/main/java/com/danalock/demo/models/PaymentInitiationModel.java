package com.danalock.demo.models;

/**
 * Created by Meraj Khan on 7/22/2019.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentInitiationModel {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public String data;

    @SerializedName("PaymentRequestToken")
    @Expose
    public String PaymentRequestToken;





}
