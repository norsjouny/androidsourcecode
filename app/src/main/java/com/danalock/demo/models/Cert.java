
package com.danalock.demo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cert {

    @SerializedName("notAfter")
    @Expose
    public String notAfter;
    @SerializedName("notBefore")
    @Expose
    public String notBefore;

}
