
package com.danalock.demo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("givenName")
    @Expose
    public String givenName;
    @SerializedName("surname")
    @Expose
    public String surname;
    @SerializedName("personalNumber")
    @Expose
    public String personalNumber;
    @SerializedName("name")
    @Expose
    public String name;

}
