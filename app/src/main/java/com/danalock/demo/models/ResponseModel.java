
package com.danalock.demo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseModel {

    @SerializedName("autoStartToken")
    @Expose
    public String autoStartToken;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("server_status")
    @Expose
    public String serverStatus;
    @SerializedName("completionData")
    @Expose
    public CompletionData completionData;
    @SerializedName("orderRef")
    @Expose
    public String orderRef;

}
