
package com.danalock.demo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompletionData {

    @SerializedName("device")
    @Expose
    public Device device;
    @SerializedName("cert")
    @Expose
    public Cert cert;
    @SerializedName("user")
    @Expose
    public User user;

}
