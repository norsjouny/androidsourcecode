package com.danalock.demo.models;

import com.danalock.webservices.danaserver.model.Device;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.polycontrol.ekey.User;

/**
 * Created by Meraj Khan on 7/19/2019.
 */
public class BankIdCompletionModel {

    @SerializedName("device")
    @Expose
    public Device device;
    @SerializedName("cert")
    @Expose
    public Cert cert;
    @SerializedName("user")
    @Expose
    public User user;
}
