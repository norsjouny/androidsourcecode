package com.danalock.demo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Meraj Khan on 7/19/2019.
 */
public class BankIdResponseModel {

    @SerializedName("autoStartToken")
    @Expose
    public String autoStartToken;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("server_status")
    @Expose
    public String serverStatus;
    @SerializedName("completionData")
    @Expose
    public BankIdCompletionModel completionData;
    @SerializedName("orderRef")
    @Expose
    public String orderRef;
}
