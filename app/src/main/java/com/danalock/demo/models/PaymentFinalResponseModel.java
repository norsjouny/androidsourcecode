package com.danalock.demo.models;

/**
 * Created by Meraj Khan on 7/22/2019.
 */


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentFinalResponseModel {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public List<Data> data = null;


    public class Data {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("payeePaymentReference")
        @Expose
        public String payeePaymentReference;
        @SerializedName("paymentReference")
        @Expose
        public Object paymentReference;
        @SerializedName("callbackUrl")
        @Expose
        public String callbackUrl;
        @SerializedName("payerAlias")
        @Expose
        public Object payerAlias;
        @SerializedName("payeeAlias")
        @Expose
        public String payeeAlias;
        @SerializedName("amount")
        @Expose
        public Integer amount;
        @SerializedName("currency")
        @Expose
        public String currency;
        @SerializedName("message")
        @Expose
        public String message;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("dateCreated")
        @Expose
        public String dateCreated;
        @SerializedName("datePaid")
        @Expose
        public Object datePaid;
        @SerializedName("errorCode")
        @Expose
        public Object errorCode;
        @SerializedName("errorMessage")
        @Expose
        public Object errorMessage;
    }
}

