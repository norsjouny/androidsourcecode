package com.danalock.demo;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.polycontrol.blescans.DLBleScanData;

import java.util.ArrayList;
import java.util.List;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Any and all rights to this source code and included documentation (hereinafter collectively referred to as “the Software”),
 * including without being limited to title and copyright, belongs to Poly-Control ApS.
 * <p>
 * The Software constitutes confidential material and shall be treated as such. It may not be disclosed to third parties.
 * <p>
 * The Software may be used only in connection with authorised sale of Danalock-products. The right of use includes the right
 * to copy, change, modify, merge and develop the Software. Any publishing, distribution, sub-licensing and sale of copies of the
 * Software requires transformation of the Software to object code form, as third parties may only be granted access to the Software
 * in its object code form.
 * <p>
 * The right of use must be executed with due respect to the rights of Poly-Control ApS, and all copies of the Software shall
 * be marked as copyrighted by Poly-Control ApS.
 * <p>
 * The right of use ceases, when the authorisation to sell Danalock-products expires.
 * <p>
 * If the Parties has entered into a separate agreement concerning the right of use, such agreement must be observed in relation
 * to any use of the Software regardless of this notice.
 * <p>
 * THE SOFTWARE IS PROVIDED “AS-IS” AND ANY USE OF THE SOFTWARE IS AT OWN RISK AND EXPENSE.
 * <p>
 * Poly-Control ApS has no obligations to correct or remedy possible faults or errors in the Software.
 * <p>
 * THERE ARE NO WARRANTIES ATTACHED TO THE SOFTWARE, NEITHER EXPRESS NOR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. UNDER NO CIRCUMSTANCES SHALL
 * POLY-CONTROL APS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT OR TORT, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR ANY USE OF THE SOFTWARE IN WHATEVER FORM.
 */
public class BleScanner {

    private BluetoothAdapter bluetoothAdapter;
    private List<ScanFilter> mBleScanFilters23;
    private ScanSettings mBleScanSettings23;
    private BluetoothLeScanner MBTLeScanner;
    private ScanCallback mScanCallBack;
    private Handler listenTimeOutHandler;
    private Runnable runner;
    private static final BleScanner ourInstance = new BleScanner();

    public static BleScanner getInstance() {
        return ourInstance;
    }

    private BleScanner() {
        initializeMarshMallowScanner();
    }

    private static boolean isBluetoothEnabled(Context c) {
        if (getInstance().bluetoothAdapter != null) {
            return getInstance().bluetoothAdapter.isEnabled();
        } else {
            BluetoothManager btMan = ((BluetoothManager) c.getSystemService(Context.BLUETOOTH_SERVICE));
            if (btMan != null) {
                getInstance().bluetoothAdapter = btMan.getAdapter();
                if (getInstance().bluetoothAdapter != null) {
                    return getInstance().bluetoothAdapter.isEnabled();
                }
            }
        }
        return false;
    }

    static void requestLocationPermissionsIfNeeded(final Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(activity, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (activity.shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION) || activity.shouldShowRequestPermissionRationale(ACCESS_COARSE_LOCATION)) {
                    new AlertDialog.Builder(activity)
                            .setMessage(activity.getString(R.string.request_permission_location_rationale))
                            .setPositiveButton(R.string.btn_dialog_okay, (dialogInterface, i) -> ActivityCompat.requestPermissions(activity, new String[] {ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION}, 0))
                            .show();
                } else {
                    ActivityCompat.requestPermissions(activity, new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION}, 0);
                }
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION}, 0);
            }
        }
    }

    public static void startBleScan(Context activity) {
        if (isBluetoothEnabled(activity)) {
            if (activity != null) {
                getInstance().initializeScannerCallback(activity);
                BluetoothManager btManager = (BluetoothManager) activity.getApplicationContext().getSystemService(Context.BLUETOOTH_SERVICE);
                if (btManager != null) {
                    getInstance().bluetoothAdapter = btManager.getAdapter();
                    if (getInstance().bluetoothAdapter != null && getInstance().bluetoothAdapter.isEnabled()) {
                        if (getInstance().MBTLeScanner == null) {
                            getInstance().MBTLeScanner = getInstance().bluetoothAdapter.getBluetoothLeScanner();
                        }
                        if (getInstance().MBTLeScanner != null) {
                            getInstance().MBTLeScanner.startScan(getInstance().mBleScanFilters23, getInstance().mBleScanSettings23, getInstance().mScanCallBack);
                        }
                    }
                }
            }
        } else {
            Toast.makeText(activity, "Turn on Bluetooth!", Toast.LENGTH_LONG).show();
        }
    }

    public static void stopBleScan() {
        if (getInstance().bluetoothAdapter != null && getInstance().bluetoothAdapter.isEnabled()) {
            if (getInstance().MBTLeScanner != null) {
                getInstance().MBTLeScanner.stopScan(getInstance().mScanCallBack);
            }
        }
    }

    public static void listenForDevice(String device_id, GetDeviceContinuation aContinuation, Long timeOutMillis) {
        mGetDeviceCallback = aContinuation;
        mListenForDeviceId = device_id;
        if (timeOutMillis != null) {

            getInstance().listenTimeOutHandler = new Handler(Looper.getMainLooper());
            getInstance().runner = () -> {
                mListenForDeviceId = null;
                GetDeviceContinuation temp = mGetDeviceCallback;
                mGetDeviceCallback = null;
                if (temp != null) {
                    temp.timedOut();
                }
            };

            getInstance().listenTimeOutHandler.postDelayed(getInstance().runner, timeOutMillis);
        }
    }

    private ScanCallback getScannerCallback() {
        return new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult r) {
                BLEDeviceFound(r.getDevice(), r.getScanRecord() != null ? r.getScanRecord().getBytes() : new byte[]{}, r.getRssi());
            }
        };
    }

    private void initializeMarshMallowScanner() {
        if (mBleScanSettings23 == null) {
            mBleScanSettings23 = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
        }
        if (mBleScanFilters23 == null) {
            mBleScanFilters23 = new ArrayList<>();
        }
    }

    private void initializeScannerCallback(final Context activity) {
        if (mScanCallBack == null) {
            mScanCallBack = getScannerCallback();
        }
    }

    private static String mListenForDeviceId = null;
    private static GetDeviceContinuation mGetDeviceCallback = null;

    private void BLEDeviceFound(BluetoothDevice device, byte[] scanRecord, int rssi) {
        DLBleScanData scanData = DLBleScanData.parseData(scanRecord, device);
        if (scanData != null) { // Making sure the scanned device is of poly-control origin

//            String specific_device_id = "e6:f4:72:xx:yy:27"; // to look for when debugging
//            if (specific_device_id.equalsIgnoreCase(scanData.getDeviceId())) {
//                Log.d("ScanTag", "the bluetooth le MAC address for device " + scanData.getDeviceId() + ", is : " + device.getAddress());
//            }


//            Log.d("ScanTag", "print all devices when they are seen by the scanner, -> device_i: " + scanData.getDeviceId() );
//            DLDevice dldevice = DLDevice.getDevice(dlkey);
//            dldevice.touch(scanData, rssi, device.getAddress());

            if (mListenForDeviceId != null && mListenForDeviceId.equalsIgnoreCase(scanData.getDeviceId())) {

                // Yay found the device
                if (listenTimeOutHandler != null) {
                    listenTimeOutHandler.removeCallbacks(runner);
                }
                mListenForDeviceId = null;
                GetDeviceContinuation temp = mGetDeviceCallback;
                mGetDeviceCallback = null;
                temp.found(device.getAddress(), scanData, rssi);
            }
        }
    }

    public abstract static class GetDeviceContinuation {
        /**
         * Implement this method to manipulate a found BLE device
         */
        public abstract void found(String macAddress, DLBleScanData scanData, int rssi);

        /**
         * Implement this to handle clean up when a device is not found after a given
         * timeout
         */
        public void timedOut() {

        }
    }
}
