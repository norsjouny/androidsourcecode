package com.danalock.demo.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.danalock.demo.R;
import com.danalock.demo.models.BarcodeResponseModel;
import com.danalock.demo.models.Data;
import com.danalock.demo.models.ProductModel;

import java.util.List;

/**
 * Created by Meraj Khan on 7/18/2019.
 */
public class CartAdapter extends ArrayAdapter<Data> {
    private List<Data> list;
    private Context context;

    private TextView ProductName,
            price,
    priceTotal,
            quantityText;
    ImageView addProduct,
            subtractMeal;

            //removeMeal;

    public CartAdapter(Context context, List<Data> myOrders) {
        super(context, 0, myOrders);
        this.list = myOrders;
        this.context = context;
    }


    public View getView(final int position, View convertView, ViewGroup parent){
        View listItemView = convertView;
        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.product_item_list,parent,false
            );
        }

        final Data currentProduct = getItem(position);

        ProductName = (TextView)listItemView.findViewById(R.id.from_name);
        price = (TextView)listItemView.findViewById(R.id.plist_price_text);
        priceTotal=listItemView.findViewById(R.id.plist_pricetotal_text);
        subtractMeal =  listItemView.findViewById(R.id.cart_minus_img);
        quantityText = (TextView)listItemView.findViewById(R.id.cart_product_quantity_tv);
        addProduct = (ImageView) listItemView.findViewById(R.id.cart_plus_img);


        // removeMeal = (TextView)listItemView.findViewById(R.id.delete_item);

        //Set the text of the product , amount and quantity


        if(currentProduct!=null){
            ProductName.setText(currentProduct.getProductName());
            price.setText(currentProduct.getPrice());
            int currentProductInt=Integer.parseInt(currentProduct.getPrice() )*currentProduct.getUnits();
            priceTotal.setText(String.valueOf(currentProductInt));
            quantityText.setText(String.valueOf(currentProduct.getUnits()));
            notifyDataSetChanged();
        }


        addProduct.setOnClickListener(view -> {

            currentProduct.addToQuantity();
            quantityText.setText( String.valueOf(currentProduct.getUnits()));
            int priceInt=Integer.parseInt(currentProduct.getPrice()) * currentProduct.getUnits();
            price.setText ( String.valueOf(priceInt));
            notifyDataSetChanged();

        });

        subtractMeal.setOnClickListener(view -> {
            currentProduct.removeFromQuantity();
            quantityText.setText(String.valueOf(currentProduct.getUnits()));
            int priceInt=Integer.parseInt(currentProduct.getPrice()) * currentProduct.getUnits();
            price.setText ( String.valueOf(priceInt));
            notifyDataSetChanged();
        });

        /* removeMeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.remove(position);
                notifyDataSetChanged();
            }
        });*/

        return listItemView;
    }

}
