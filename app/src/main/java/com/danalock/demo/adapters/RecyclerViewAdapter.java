package com.danalock.demo.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danalock.demo.R;
import com.danalock.demo.interfaces.OnItemClick;
import com.danalock.demo.models.ProductModel;

import java.util.List;

/**
 * Created by Meraj Khan on 7/17/2019.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<ProductModel> productList;
    private OnItemClick mCallback;
    public RecyclerViewAdapter(List<ProductModel> productList,OnItemClick itemClick) {
        this.productList = productList;
        this.mCallback=itemClick;
    }

//    public void increment(){
//        int currentNos = Integer.parseInt(quantity.getText().toString()) ;
//        quantity.setText(String.valueOf(++currentNos));
//        mCallback.onClick(quantity.getText().toString());
//    }
//    public void decrement(){
//        int currentNos = Integer.parseInt(quantity.getText().toString()) ;
//        quantity.setText(String.valueOf(--currentNos));
//        mCallback.onClick(quantity.getText().toString());
//    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item_list, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ProductModel productModel = productList.get(position);
        holder.productName.setText(productModel.getProductName());
        holder.price.setText(productModel.getProductPrice());
        holder.quantity.setText(productModel.getQuantity());
     }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView productName, price, quantity;
        public ImageView plusImage,minusImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.from_name);
            price = itemView.findViewById(R.id.plist_price_text);
            quantity = itemView.findViewById(R.id.cart_product_quantity_tv);
            plusImage=itemView.findViewById(R.id.cart_plus_img);
            minusImage=itemView.findViewById(R.id.cart_minus_img);
        }
    }
}

